"""
Extraits d'implémentation d'une fonctionnalité de filtres par Python du fichier CSV importé.
Remplacé par un filtre SQL (option "where").
Problèmes rencontrés : cf. FIXME.
"""
import re
from typing import Union

row_filters: list[Union[str,re.Pattern]]

def __init__(self, filters: dict[str,Union[str,re.Pattern]] = None):
    # Filters against CSV raw values (strings)
    if filters is None:
        self.filters = None
    elif isinstance(filters, dict):
        self.filters = filters
    elif isinstance(filters, list):
        self.filters = {}
        for filt in filters:
            pos = filt.index("=")
            self.filters[filt[0:pos]] = filt[pos+1:]
    else:
        raise ValueError(f"invalid type for filters: {type(filters)}")


def prepare_row_filters(self, file_columns: list[Column]|None):
    rowcolumns = file_columns if file_columns is not None else self.columns
    self.row_filters = [None] * len(rowcolumns)

    for name, value in self.filters.items():
        slug = name if self.noslug else self.slugen_identifier(name)

        if not isinstance(value, (str, re.Pattern)):
            raise ValueError(f"invalid type \"{type(value)}\" for csv filter on column \"{name}\": expected str or Pattern")

        # search column
        found = False
        for index, column in enumerate(rowcolumns):
            if column.slug == slug:
                found = True
                self.row_filters[index] = value
                break

        if not found:
            raise ValueError(f"column not found for filter \"{name}\"")


def copy_from(self):
    with self.cursor.copy(query) as copy:
        if self.filters:
            # Row-by-row copy
            for row in self.csv_reader:
                #FIXME: le parsing CSV ne peut pas distinguer entre la chaîne vide (échapée : `""`) et la valeur nulle (``)
                #FIXME: apply filters
                copy.write(row)
        else:
            # Direct file copy
            while data := self.txt_reader.read(self.blocksize):
                copy.write(data)


def from_csv(self):
    with open(self.file, "r", newline="", encoding=self.encoding) as self.txt_reader:
        self.prepare_csv_params()
        file_columns = self.prepare_columns_from_file()
        self.prepare_table()
        
        if self.filters:
            self.prepare_row_filters(file_columns)
        self.copy_from()
