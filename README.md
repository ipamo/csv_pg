csv-pg
======

A Python module to import/export CSV files efficiently to/from a Postgresql database.

Key features:
- Detect CSV file dialect automatically
- Create target database table automatically
- Handle localized date/time and decimal formats
- Filter CSV file rows during import


## Usage examples

Installation:

    pip install csv-pg

Import CSV file `tests/samples/misc.csv` to table `misc` (recreate table if already exists):

    python -m csv_pg from_csv tests/samples/misc.csv --recreate

Export table `misc` to CSV file `misc.local.csv` (recreate file if already exists):
    
    python -m csv_pg to_csv misc.local.csv misc --recreate


## Contributors guidelines

Prepare environment:

    # Debian: sudo apt install libpq-dev
    python -m venv .venv       # Debian: python3 -m venv .venv
    .\.venv\Scripts\activate   # Linux: source .venv/bin/activate
    pip install .

Create test database:

    createdb test_csv_pg -E UTF8 -l en_US.UTF8 -T template0

Run tests:

    python -m unittest


## Alternatives / inspirations

- [Pandas](https://apoor.medium.com/quickly-load-csvs-into-postgresql-using-python-and-pandas-9101c274a92f):
    - Have to manually call Postgresql COPY command?

- [Django Postgres Copy](https://github.com/palewire/django-postgres-copy)
    - Depends on Django, and force import to a Django model (cannot simply load to a temporary table)
    - No filter during import?
