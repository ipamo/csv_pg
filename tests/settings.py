import os

DATABASES = {
    "default": {
        "NAME": os.environ.get("PGDATABASE_TEST", "test_csv_pg"),
        "HOST": os.environ.get("PGHOST", None),
        "PORT": os.environ.get("PGPORT", None),
        "USER": os.environ.get("PGUSER", None),
        "PASSWORD": os.environ.get("PGPASSWORD", None),
    }
}

