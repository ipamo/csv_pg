from unittest import TestCase
from csv_pg.column import Column

class Case(TestCase):
    def test_name_parsing(self):
        self.assertEqual("my_column:decimal(10,2)-notnull",
            str(Column.from_spec("my_column : decimal(10, 2) - notnull")))

        self.assertEqual("my_column:text-default(')(-'::text)-ix",
            str(Column.from_spec("my_column:text-ix-notnull-default(')(-'::text)")))
    
        self.assertEqual("my_column:varchar(200)-filename",
            str(Column.from_spec("my_column:varchar(200)-filename")))

        try:
            Column.from_spec("my_column:text-default())-ix-notnull")
            self.fail()
        except ValueError as err:
            self.assertEqual("syntax error: cannot close parenthis in \"default())\"", str(err))

        self.assertEqual("my_column:text-seq-ix",
            str(Column.from_spec("my_column:text-default(nextval())-ix-notnull")))

        self.assertEqual("my_column:text-seq",
            str(Column.from_spec("my_column:text-default(nextval('-)(>''<'))")))
        
        self.assertEqual("my_column:text-filename",
            str(Column.from_spec("my_column:text-default('__filename__'::text)")))

        self.assertEqual("my_column:text-filepath",
            str(Column.from_spec("my_column:text-default('__filepath__'::text)")))
