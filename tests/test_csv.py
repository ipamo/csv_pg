from unittest import TestCase
import csv
from io import StringIO

class Case(TestCase):
    def test_escapechar_equal_quotechar(self):
        def parse_row(line: str):
            return next(csv.reader(StringIO(line), delimiter=';', quotechar='"', escapechar=None, doublequote=True))
            
        # simple
        self.assertEqual(parse_row('1;2;3;4;5'), ['1', '2', '3', '4', '5'])

        # simple escape
        self.assertEqual(parse_row('1;"2";3;4;5'), ['1', '2', '3', '4', '5'])

        # escaped escapechar
        self.assertEqual(parse_row('1;"2""2";3;4;5'), ['1', '2"2', '3', '4', '5'])

        # escaped delimiter
        self.assertEqual(parse_row('1;"2;2";3;4;5'), ['1', '2;2', '3', '4', '5'])

        # empty escaped string
        self.assertEqual(parse_row('1;"";3;4;5'), ['1', '', '3', '4', '5'])

        # empty escaped string
        self.assertEqual(parse_row('1;"";3;4;5'), ['1', '', '3', '4', '5'])


    def test_escapechar_not_quotechar(self):
        def parse_row(line: str):
            return next(csv.reader(StringIO(line), delimiter=';', quotechar='"', escapechar="\\", doublequote=False))
            
        # simple
        self.assertEqual(parse_row('1;2;3;4;5'), ['1', '2', '3', '4', '5'])

        # simple escape
        self.assertEqual(parse_row('1;"2";3;4;5'), ['1', '2', '3', '4', '5'])

        # escaped escapechar
        self.assertEqual(parse_row('1;"2\\"2";3;4;5'), ['1', '2"2', '3', '4', '5'])

        # escaped delimiter
        self.assertEqual(parse_row('1;"2;2";3;4;5'), ['1', '2;2', '3', '4', '5'])

        # empty escaped string
        self.assertEqual(parse_row('1;"";3;4;5'), ['1', '', '3', '4', '5'])

        # empty escaped string
        self.assertEqual(parse_row('1;"";3;4;5'), ['1', '', '3', '4', '5'])
